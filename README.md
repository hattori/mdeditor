#mdeditor
### 演示地址
[http://www.qinshenxue.com/demo/mdeditor/index.html](http://www.qinshenxue.com/demo/mdeditor/index.html)
### 使用方法见博客
[http://www.qinshenxue.com/article.aspx?id=36](http://www.qinshenxue.com/article.aspx?id=36)

## 已支持语法
### 标题
![](readme/h16.gif)
### 段落
![](readme/p.gif)
### 链接
![](readme/a.gif)
### 列表（无序列表，有序列表）
![](readme/ul.gif)
![](readme/ol.gif)
### 图片
![](readme/img.gif)
### 行内代码
![](readme/inlinecode.gif)
### 代码块
![](readme/code.gif)